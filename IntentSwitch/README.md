User changes the settings to choose the color and the app displays that color. User can reset the settings.

Two activities are created and switching between them via intents is shown. Use of SharedPreferences is demonstrated.
