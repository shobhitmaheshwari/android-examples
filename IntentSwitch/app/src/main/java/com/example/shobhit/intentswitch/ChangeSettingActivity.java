/**
 * Created by shobhit on 1/15/16.
 */
package com.example.shobhit.intentswitch;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class ChangeSettingActivity extends AppCompatActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_changesetting);
	}

    public void setColor(View v){
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);

        Button button = (Button) v;

        //add color to settings
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("color", button.getText().toString());
        editor.commit();

        //Go back to other activity
        finish();
    }
}
