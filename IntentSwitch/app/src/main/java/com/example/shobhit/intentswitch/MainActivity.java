package com.example.shobhit.intentswitch;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.Shape;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

	Context context;
	SharedPreferences settings;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		context = getApplicationContext();
		settings = PreferenceManager.getDefaultSharedPreferences(this);
	}

	@Override
	protected void onResume(){
		reInitialize();
		super.onResume();
	}
	/*
	This will change button text and rectangle shape color and text as appropriate
	 */
	private void reInitialize(){

		String color = settings.getString("color", null);

		Button button = (Button) findViewById(R.id.button);
		TextView rectangleView = (TextView) findViewById(R.id.rectangleView);
		Drawable background = rectangleView.getBackground();

		if(color != null){

			button.setText("Change Color");

			switch (color){
				case "Red":{
					setColor(R.color.red, background);
					rectangleView.setText("Red Color");
					break;
				}
				case "Green":{
					setColor(R.color.green, background);
					rectangleView.setText("Green Color");
					break;
				}
			}
		} else {
			//default
			button.setText("Set Color");
			setColor(R.color.white, background);
			rectangleView.setText("There is no color");
		}
	}

	/*
	This will change the rectangle color
	 */
	private void setColor(int id, Drawable background){
		if (background instanceof ShapeDrawable) {
			((ShapeDrawable)background).getPaint().setColor(ContextCompat.getColor(context, id));
		} else if (background instanceof GradientDrawable) {
			((GradientDrawable)background).setColor(ContextCompat.getColor(context, id));
		}
	}

	/*
	Go to ChangeSettingActivity to choose color
	 */
	public void changeSetting(View v){
		Intent intent = new Intent(this, ChangeSettingActivity.class);
		startActivity(intent);
	}

	/*
	Clear the setting and reinitialize
	 */
	public void reset(View v){
		SharedPreferences.Editor editor = settings.edit();

		String color = settings.getString("color", null);
		if(color != null){
			editor.remove("color");
			editor.commit();
			reInitialize();
		}
	}
}